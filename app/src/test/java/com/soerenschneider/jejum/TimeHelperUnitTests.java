package com.soerenschneider.jejum;

import org.junit.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.Assert.*;


public class TimeHelperUnitTests {

    @Test
    public void addition_isCorrect() {
        ZonedDateTime start = ZonedDateTime.parse("2018-12-03T20:15:30+01:00[Europe/Berlin]");
        ZonedDateTime target =  ZonedDateTime.parse("2018-12-04T12:15:00+01:00[Europe/Berlin]");

        int fastingWindowSeconds = 16 * 60 * 60;

        ZonedDateTime computed = Bla.getTargetDate(fastingWindowSeconds, start);
        assertEquals(target, computed);
    }

    @Test
    public void targetReached() {
        ZonedDateTime target =  ZonedDateTime.parse("2018-12-04T12:15:30+01:00[Europe/Berlin]");

        assertTrue(Bla.achievedGoal(target, target));
    }

    @Test
    public void targetReachedPos() {
        ZonedDateTime target =  ZonedDateTime.parse("2018-12-04T12:15:30+01:00[Europe/Berlin]");
        ZonedDateTime actual =  ZonedDateTime.parse("2018-12-04T12:15:31+01:00[Europe/Berlin]");

        assertTrue(Bla.achievedGoal(target, actual));
    }

    @Test
    public void targetReachedNeg() {
        ZonedDateTime target =  ZonedDateTime.parse("2018-12-04T12:15:30+01:00[Europe/Berlin]");
        ZonedDateTime actual =  ZonedDateTime.parse("2018-12-04T12:15:29+01:00[Europe/Berlin]");

        assertFalse(Bla.achievedGoal(target, actual));
    }

    @Test
    public void bla() {
        ZonedDateTime target =  ZonedDateTime.parse("2018-12-04T12:15:30+01:00[Europe/Berlin]");

        long utc = Bla.toUtc(target);
        Optional<ZonedDateTime> actual = Bla.fromUtc(utc);

        assertEquals(target, actual.get());
    }

    @Test
    public void testIsYesterday() {
        ZonedDateTime dayBeforeYesterday = ZonedDateTime.now(ZoneId.systemDefault()).minusDays(2);
        ZonedDateTime yesterday = ZonedDateTime.now(ZoneId.systemDefault()).minusDays(1);
        ZonedDateTime today = ZonedDateTime.now(ZoneId.systemDefault());

        assertTrue(Bla.isYesterday(yesterday));
        assertFalse(Bla.isYesterday(today));
        assertFalse(Bla.isYesterday(dayBeforeYesterday));
    }

    @Test
    public void testIsToday() {
        ZonedDateTime yesterday = ZonedDateTime.now(ZoneId.systemDefault()).minusDays(1);
        ZonedDateTime today = ZonedDateTime.now(ZoneId.systemDefault());
        ZonedDateTime tomorrow = ZonedDateTime.now(ZoneId.systemDefault()).plusDays(1);
        
        assertFalse(Bla.isToday(yesterday));
        assertTrue(Bla.isToday(today));
        assertFalse(Bla.isToday(tomorrow));
    }

    @Test
    public void testIsTomorow() {
        ZonedDateTime yesterday = ZonedDateTime.now(ZoneId.systemDefault()).minusDays(1);
        ZonedDateTime today = ZonedDateTime.now(ZoneId.systemDefault());
        ZonedDateTime tomorrow = ZonedDateTime.now(ZoneId.systemDefault()).plusDays(1);
        ZonedDateTime dayAfterTomorrow = ZonedDateTime.now(ZoneId.systemDefault()).plusDays(2);

        assertFalse(Bla.isTomorrow(yesterday));
        assertFalse(Bla.isTomorrow(today));
        assertTrue(Bla.isTomorrow(tomorrow));
        assertFalse(Bla.isTomorrow(dayAfterTomorrow));
    }
}