package com.soerenschneider.jejum;

import org.junit.Test;

import java.time.ZonedDateTime;

import static junit.framework.TestCase.assertTrue;

public class DateRangeTest {
    @Test
    public void openrange() {
        DateRange r = new DateRange();
        ZonedDateTime now = Bla.now();
        assertTrue(now.isAfter(r.from));
        assertTrue(now.isBefore(r.to));
    }

    @Test
    public void year() {
        ZonedDateTime now = Bla.now();
        DateRange r = new DateRange(now.getYear());
        assertTrue(now.isAfter(r.from));
        assertTrue(now.isBefore(r.to));
    }

    @Test
    public void year_previous() {
        ZonedDateTime now = Bla.now();
        DateRange r = new DateRange(now.minusYears(1).getYear());
        assertTrue(now.isAfter(r.from));
        assertTrue(now.isAfter(r.to));
    }

    @Test
    public void year_next() {
        ZonedDateTime now = Bla.now();
        DateRange r = new DateRange(now.plusYears(1).getYear());
        assertTrue(now.isBefore(r.from));
        assertTrue(now.isBefore(r.to));
    }

    @Test
    public void month() {
        ZonedDateTime now = Bla.now();
        DateRange r = new DateRange(now.getYear(), now.getMonthValue());
        assertTrue(now.isAfter(r.from));
        assertTrue(now.isBefore(r.to));
    }

    @Test
    public void month_previous() {
        ZonedDateTime now = Bla.now();
        ZonedDateTime offset = now.minusMonths(1);
        DateRange r = new DateRange(offset.getYear(), offset.getMonthValue());

        assertTrue(now.isAfter(r.from));
        assertTrue(now.isAfter(r.to));
    }

    @Test
    public void month_next() {
        ZonedDateTime now = Bla.now();
        ZonedDateTime offset = now.plusMonths(1);
        DateRange r = new DateRange(offset.getYear(), offset.getMonthValue());

        assertTrue(now.isBefore(r.from));
        assertTrue(now.isBefore(r.to));
    }

    @Test
    public void week() {
        ZonedDateTime now = Bla.now();
        DateRange r = new DateRange(now.getYear(), now.getMonthValue(), now.getDayOfMonth());

        assertTrue(now.isAfter(r.from));
        assertTrue(now.isBefore(r.to));
    }

    @Test
    public void week_prev() {
        ZonedDateTime now = Bla.now();
        ZonedDateTime offset = now.minusWeeks(1);
        DateRange r = new DateRange(offset.getYear(), offset.getMonthValue(), offset.getDayOfMonth());

        assertTrue(now.isAfter(r.from));
        assertTrue(now.isAfter(r.to));
    }

    @Test
    public void week_next() {
        ZonedDateTime now = Bla.now();
        ZonedDateTime offset = now.plusWeeks(1);
        DateRange r = new DateRange(offset.getYear(), offset.getMonthValue(), offset.getDayOfMonth());

        assertTrue(now.isBefore(r.from));
        assertTrue(now.isBefore(r.to));
    }

    @Test
    public void bla() {
        String sunday_rfc3339 = "2019-01-20T10:03:59.655+01:00";
        ZonedDateTime sunday = ZonedDateTime.parse(sunday_rfc3339);
        ZonedDateTime last_sunday = sunday.minusWeeks(1);

        String monday_rfc3339 = "2019-01-14T10:03:59.655+01:00";
        ZonedDateTime monday = ZonedDateTime.parse(monday_rfc3339);
        ZonedDateTime next_monday = monday.plusWeeks(1);

        DateRange r = new DateRange(sunday.getYear(), sunday.getMonthValue(), sunday.getDayOfMonth());
        assertTrue(sunday.isAfter(r.from));
        assertTrue(sunday.isBefore(r.to));

        assertTrue(monday.isAfter(r.from));
        assertTrue(monday.isBefore(r.to));

        assertTrue(last_sunday.isBefore(r.from));
        assertTrue(last_sunday.isBefore(r.to));

        assertTrue(next_monday.isAfter(r.from));
        assertTrue(next_monday.isAfter(r.to));
    }


}
