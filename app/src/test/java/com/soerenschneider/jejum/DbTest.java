package com.soerenschneider.jejum;

import android.content.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class DbTest {

    @Before
    public void before() {
        Context c = RuntimeEnvironment.application;
        DbHelper helper = new DbHelper(c);
        helper.deleteAll();
        assertTrue(helper.getRecords().isEmpty());
    }

    @Test
    public void bla() {
        Context c = RuntimeEnvironment.application;
        DbHelper helper = new DbHelper(c);

        Record old = new Record(60, Bla.now());
        Record mostRecent = new Record(60, Bla.now());
        helper.insertRecord(old);
        helper.insertRecord(mostRecent);

        assertTrue(helper.getMostRecentRecord().isPresent());
        assertEquals(mostRecent, helper.getMostRecentRecord().get());
        System.out.println(helper.getRecords());
    }

    @Test
    public void blu() {
        Context c = RuntimeEnvironment.application;
        DbHelper helper = new DbHelper(c);

        int windowLength = 16 * 3600;
        Record r = new Record(windowLength, Bla.now());
        helper.insertRecord(r);

        Record read = helper.getMostRecentRecord().get();

        assertEquals(read, helper.getMostRecentRecord().get());
        System.out.println(helper.getRecords());
    }
}
