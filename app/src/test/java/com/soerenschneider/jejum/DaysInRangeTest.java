package com.soerenschneider.jejum;

import org.junit.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;

import static org.junit.Assert.*;

public class DaysInRangeTest {
    @Test
    /**
     * Use a startdate right before midnight with a relatively short windowLength (30 seconds). The target date
     * is way shorter than 24h but the target date lies in the new day. The function is expected to return
     * the day of the start and the new day the target overlaps into, even though it's < 24h.
     */
    public void overlapping() {
        ZonedDateTime almostMidnight = ZonedDateTime.of(2019, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
        int windowLengthSeconds = 30;
        Map<String, ZonedDateTime> result = Bla.getDaysInRange(almostMidnight, windowLengthSeconds).getAllowedDays();
        assertTrue(result.size() > 1);
    }

    @Test
    /**
     * Use a startdate shortly after midnight and a relatively long windowLength of 16 hours. The result is
     * expected to only contain the day of the start.
     */
    public void notOverlapping() {
        ZonedDateTime rightAfterMidnight = ZonedDateTime.of(2019, 12, 31, 0, 5, 0, 0, ZoneId.systemDefault());
        int windowLengthSeconds = 16 * 60 * 60; // 16 hours
        Map<String, ZonedDateTime> result = Bla.getDaysInRange(rightAfterMidnight, windowLengthSeconds).getAllowedDays();
        assertTrue(result.size() == 1);
    }

    @Test
    /**
     * Use a startdate right before midnight with and a long windowLength of 26 hours. The result is
     * expected to contain a span of three days from the start.
     */
    public void overlappingLong() {
        ZonedDateTime almostMidnight = ZonedDateTime.of(2019, 12, 31, 23, 59, 59, 0, ZoneId.systemDefault());
        int windowLengthSeconds = 26 * 60 * 60; // 26 hours
        Map<String, ZonedDateTime> result = Bla.getDaysInRange(almostMidnight, windowLengthSeconds).getAllowedDays();
        assertTrue(result.size() == 3);
    }
}