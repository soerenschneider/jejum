package com.soerenschneider.jejum;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class MyAdapter extends CursorAdapter {
    public MyAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.listlayout, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView tv_date = view.findViewById(R.id.list_date);
        TextView tv_target = view.findViewById(R.id.list_target);
        TextView tv_length = view.findViewById(R.id.list_duration);
        TextView tv_diff = view.findViewById(R.id.list_diff);

        // Extract properties from cursor

        int diff = cursor.getInt(cursor.getColumnIndexOrThrow("diff"));
        String prefix = "";
        if (diff > 0) {
            prefix = "+";
        }
        tv_diff.setText(prefix+diff+"m");

        long start = cursor.getLong(cursor.getColumnIndexOrThrow("start"));
        long target = cursor.getLong(cursor.getColumnIndexOrThrow("target"));
        int windowLength = cursor.getInt(cursor.getColumnIndexOrThrow("windowLength"));

        // Populate fields with extracted properties
        tv_date.setText(Bla.formatDayTime(Bla.fromUtc(start).get()));
        tv_target.setText(Bla.formatDayTime(Bla.fromUtc(target).get()));
        tv_length.setText(windowLength / 60 + "m");
    }
}