package com.soerenschneider.jejum;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "jejum";
    public static final int DATABASE_VERSION = 1;

    private String createStatement = "" +
            "CREATE TABLE " + DATABASE_NAME + " (" +
            "id INTEGER PRIMARY KEY," +
            "windowLength INTEGER NOT NULL," +
            "start BIGINT NOT NULL, "+
            "target BIGINT NOT NULL," +
            "actual BIGINT," +
            "diff INTEGER)";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createStatement);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
        onCreate(db);
        db.close();
    }

    void deleteCurrent() {
        Optional<Record> mr = this.getMostRecentRecord();
        if (mr.isPresent()) {
            delete(mr.get().getId());
        }
    }

    void delete(int id) {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(DATABASE_NAME, "id=?", new String[] {id+""});
            db.close();
    }

    void insertRecord(Record record) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = record.toValues();

        db.insert(DATABASE_NAME, null, values);
        db.close(); // Closing database connection
    }

    public List<JejumStat> getStats() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT diff, start, target FROM " + DATABASE_NAME + " WHERE diff IS NOT NULL ORDER BY start";

        List<JejumStat> stats = new ArrayList<>();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() != true) {
                int diff =  cursor.getInt(cursor.getColumnIndex("diff"));
                long start=  cursor.getLong(cursor.getColumnIndex("start"));
                long target =  cursor.getLong(cursor.getColumnIndex("target"));

                stats.add(new JejumStat(diff, start, target));
            }
        }
        db.close();

        return stats;
    }

    void updateRecord(Record record) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = record.toValues();

        db.update(DATABASE_NAME, contentValues, "id = ?", new String[] {record.getId()+""});
        db.close();
    }

    // code to get all contacts in a list view
    public List<Record> getRecords() {
        List<Record> contactList = new ArrayList<Record>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + DATABASE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Record rec = new Record(c);

                // Adding contact to list
                contactList.add(rec);
            } while (c.moveToNext());
        }

        c.close();
        db.close();

        // return contact list
        return contactList;
    }


    public Optional<Record> getMostRecentRecord() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + DATABASE_NAME + " ORDER BY id DESC LIMIT 1", null);

        if (c == null || c.getCount() < 1) {
            return Optional.empty();
        }

        c.moveToFirst();

        Record rec = new Record(c);
        Log.i("bla", rec.toString());

        c.close();
        db.close();
        return Optional.of(rec);
    }

    /**
     * Returns the record before the most recent one.
     *
     * @return
     */
    public Optional<Record> getPreviousRecord() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + DATABASE_NAME + " ORDER BY id DESC LIMIT 2", null);

        if (c == null || c.getCount() < 2) {
            return Optional.empty();
        }

        c.moveToLast();

        Record rec = new Record(c);
        Log.i("bla", rec.toString());

        c.close();
        db.close();
        return Optional.of(rec);
    }
}
