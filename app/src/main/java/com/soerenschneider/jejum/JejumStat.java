package com.soerenschneider.jejum;

import java.time.ZonedDateTime;

public class JejumStat {
    int diff;
    ZonedDateTime start;
    ZonedDateTime target;
    boolean successful;

    public JejumStat() {
    }

    public JejumStat(int diff, long start, long target) {
        this.diff = diff;
        this.successful = diff >= 0;
        this.start = Bla.fromUtc(start).get();
        this.target = Bla.fromUtc(target).get();
    }

    @Override
    public String toString() {
        return "JejumStat{" +
                "diff=" + diff +
                ", start=" + start +
                ", target=" + target +
                ", successful=" + successful +
                '}';
    }
}
