package com.soerenschneider.jejum;

import android.content.DialogInterface;
import android.view.View;

public interface AppState {
    JejumState getFastingState();

    View.OnClickListener getButtonListener();
    DialogInterface.OnClickListener getDialogButtonListener();

    void updateElements();
    void fromState(JejumState state);
}
