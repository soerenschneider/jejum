package com.soerenschneider.jejum;

import android.content.ContentValues;
import android.database.Cursor;

import java.time.ZonedDateTime;
import java.util.Optional;


public class Record {
    private Integer id;
    private Integer windowLength;
    private Long start;
    private Long target;
    private Long actual;
    private Integer diff;

    public Record(int windowLengthSeconds, ZonedDateTime start) {
        setWindowLength(windowLengthSeconds);
        setStart(start);
        setTarget(Bla.getTargetDate(windowLengthSeconds, start));
    }

    public Record(Cursor c) {
        id = Integer.valueOf(c.getInt(0));
        windowLength = Integer.valueOf(c.getInt(1));
        start = Long.valueOf(c.getLong(2));
        target = Long.valueOf(c.getLong(3));
        if (! c.isNull(4)) {
            actual = Long.valueOf(4);
        }

        if (! c.isNull(5)) {
            diff = Integer.valueOf(5);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        if (null == this.id) {
            this.id = id;
        }
    }

    public ZonedDateTime getStart() {
        return Bla.fromUtc(start).get();
    }

    public void setStart(ZonedDateTime start) {
        this.start = Bla.toUtc(start);
    }

    public Integer getWindowLength() {
        return windowLength;
    }

    public void setWindowLength(Integer windowLength) {
        this.windowLength = windowLength;
    }

    public ZonedDateTime getTarget() {
        return Bla.fromUtc(target).get();
    }

    public void setTarget(ZonedDateTime target) {
        this.target = Bla.toUtc(target);
    }

    public Optional<ZonedDateTime> getActual() {
        return Bla.fromUtc(actual);
    }

    public void setActual(ZonedDateTime actual) {
        this.actual = Bla.toUtc(actual);
        int diff = (int) Bla.diff(Bla.fromUtc(target).get(), actual).toMinutes();
        this.setDiff(diff);
    }

    public Integer getDiff() {
        return diff;
    }

    public void setDiff(Integer diff) {
        this.diff = diff;
    }

    public ContentValues toValues() {
        ContentValues values = new ContentValues();
        if (id != null) {
            values.put("id", id);
        }
        values.put("start", start);
        values.put("windowLength", windowLength);
        values.put("target", target);
        if (actual != null) {
            values.put("actual", actual);
        }
        if (diff != null) {
            values.put("diff", diff);
        }

        return values;
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", windowLength=" + getWindowLength() +
                ", start=" + getStart() +
                ", target=" + getTarget() +
                ", actual=" + getActual() +
                ", diff=" + getDiff() +
                '}';
    }
}
