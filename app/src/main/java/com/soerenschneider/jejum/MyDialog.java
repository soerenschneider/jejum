package com.soerenschneider.jejum;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

public class MyDialog extends AlertDialog implements DialogInterface.OnClickListener {

    private NumberPicker dayPicker;
    private TimePicker timePicker;

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }

    @Override
    public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> data, @Nullable Menu menu, int deviceId) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public MyDialog(Context context, TimePickerDialog.OnTimeSetListener listener, AllowedDays allowedDays, AllowedDays.Preference displayPreference, int hourOfDay, int minute, boolean is24HourView) {
        super(context);

        final Context themeContext = getContext();
        final LayoutInflater inflater = LayoutInflater.from(themeContext);
        final View view = inflater.inflate(R.layout.dialog_define_target, null);

        this.dayPicker = view.findViewById(R.id.dialog_define_target_daypicker);
        dayPicker.setMinValue(0);
        dayPicker.setMaxValue(allowedDays.getAllowedDays().size() - 1);
        dayPicker.setValue(allowedDays.getIndexOfPreferencedDay(displayPreference));
        dayPicker.setDisplayedValues(allowedDays.toDisplayableValues());

        this.timePicker = view.findViewById(R.id.dialog_define_target_timepicker);

        setView(view);
        setButton(BUTTON_POSITIVE, "OK", this);
        setButton(BUTTON_NEGATIVE, themeContext.getString(R.string.cancel), this);
    }
}
