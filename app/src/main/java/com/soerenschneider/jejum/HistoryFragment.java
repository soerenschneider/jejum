package com.soerenschneider.jejum;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ListView;

public class HistoryFragment extends Fragment {


    private CalendarView calendarView;

    private DbHelper db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View child = inflater.inflate(R.layout.fragment_history, container, false);
        ListView lv = child.findViewById(R.id.fragment_history_listview);
        DbHelper db = new DbHelper(getContext());
        Cursor c = db.getReadableDatabase().rawQuery("SELECT id as _id, diff, start, target, windowLength from " + db.DATABASE_NAME + " where diff is not null order by start desc", null);
        MyAdapter adapter = new MyAdapter(getContext(), c);
        lv.setAdapter(adapter);
        return child;
    }

}
