package com.soerenschneider.jejum;

import android.os.CountDownTimer;

public abstract class JejumCountdownTimer extends CountDownTimer {
    protected boolean isFinished = false;

    public JejumCountdownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }
}
