package com.soerenschneider.jejum;

import android.util.Log;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

public class ManuallyPickedTargetTime {
    public boolean useManuallyPickedTarget = false;

    public int hour;
    public int minute;

    public ZonedDateTime day;

    public Optional<ZonedDateTime> getTarget() {
        if (day != null) {
            ZonedDateTime pickedDateTime = ZonedDateTime.of(day.getYear(), day.getMonthValue(), day.getDayOfMonth(), hour, minute, 0, 0, ZoneId.systemDefault());
            ZonedDateTime now = Bla.now().withSecond(0).withNano(0);

            if (pickedDateTime.isAfter(now)) {
                Log.i("ManualPicker", "User picked date that's after now, resetting time to now. ");
                return Optional.of(now);
            }

            return Optional.of(pickedDateTime);
        }
        return Optional.empty();
    }
}
