package com.soerenschneider.jejum;

public enum JejumState {
    FASTING,
    RESTING;
}
