package com.soerenschneider.jejum;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class FastingFragment extends Fragment {
    private static final String DEFAULT_IMMINENT_NOTIFICATION_OFFSET = "60";
    private Optional<Record> readRecord = Optional.empty();
    private Button action;
    private TextView countdown;
    private TextView startDate;
    private TextView targetDate;
    private ProgressBar progressBar;
    private JejumCountdownTimer timer;
    private DbHelper dbHelper;
    private AppState state;
    private Map<JejumState, AppState> states = new HashMap<>();
    private SharedPreferences preferences;

    private void fetchLatestEntry() {
        this.readRecord = dbHelper.getMostRecentRecord();
    }

    private void initializeUiElements(View v) {
        this.action = v.findViewById(R.id.button4);
        this.progressBar = v.findViewById(R.id.progressBar);
        this.countdown = v.findViewById(R.id.countdown);
        this.startDate = v.findViewById(R.id.text1);
        this.targetDate = v.findViewById(R.id.text2);
        this.action.setText("Interrupt");

        startDate.setText("Start");
        targetDate.setText("Target");
        countdown.setText("Countdown");

        states.put(JejumState.FASTING, new FastingState());
        states.put(JejumState.RESTING, new RestingState());
    }

    private void scheduleNotification(ZonedDateTime notificationDate, NotificationType type) {
        Intent alarmIntent = new Intent(getContext(), NotifierService.class);
        alarmIntent.putExtra("type", type.name());

        // Schedule imminent notification
        if (NotificationType.IMMINENT == type) {
            String key = getResources().getString(R.string.prefs_notifications_imminent_offset);
            int minutes = Integer.valueOf(preferences.getString(key, DEFAULT_IMMINENT_NOTIFICATION_OFFSET));
            notificationDate = notificationDate.minusMinutes(minutes);

            alarmIntent.putExtra(getResources().getString(R.string.prefs_notifications_imminent_offset), minutes);
        }

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getContext(), type.code, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) this.getContext().getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);

        //manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 3000 + Bla.now().toInstant().toEpochMilli(), pendingIntent);
        manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, notificationDate.toInstant().toEpochMilli(), pendingIntent);
    }

    public void scheduleNotifications(ZonedDateTime target) {
        if (preferences.getBoolean(getResources().getString(R.string.prefs_notifications_imminent_notification), false)) {
            scheduleNotification(target, NotificationType.IMMINENT);
        }

        scheduleNotification(target, NotificationType.ACHIEVED);
    }

    public void cancelScheduledNotifications() {
        for (NotificationType type : NotificationType.values()) {
            Intent alarmIntent = new Intent(getContext(), NotifierService.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getContext(), type.code, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager manager = (AlarmManager) this.getContext().getSystemService(Context.ALARM_SERVICE);
            manager.cancel(pendingIntent);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View child = inflater.inflate(R.layout.layout, container, false);

        this.initializeUiElements(child);
        this.dbHelper = new DbHelper(getContext());
        this.preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        progressBar.setProgress(0);

        fetchLatestEntry();
        update();

        return child;
    }



    private DateTimePickerDialog manuallyTargetPicker;

    private void showOverrideTargetReachedDialog() {
        DialogInterface.OnClickListener getDialogButtonListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int choice) {
                switch (choice) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Record r = readRecord.get();
                        if (manuallyTargetPicker.getManuallyPickedTarget().getTarget().isPresent()) {
                            r.setActual(manuallyTargetPicker.getManuallyPickedTarget().getTarget().get());
                        } else {
                            r.setActual(r.getTarget());
                        }
                        dbHelper.updateRecord(r);
                        fetchLatestEntry();
                        break;
                }
            }
            };

        manuallyTargetPicker = new DateTimePickerDialog(getDialogButtonListener);

                    AlertDialog dialog = manuallyTargetPicker.getBuilder().create();
                    dialog.show();

    }

    private void evaluateState() {
        if (! readRecord.isPresent()) {
            return;
        }

        Record r = readRecord.get();

        if (! r.getActual().isPresent() && Bla.achievedGoal(r.getTarget())) {
            showOverrideTargetReachedDialog();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        evaluateState();
        scheduleTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        cancelTimer();
    }

    private void updateState() {
        JejumState desiredState = JejumState.RESTING;
        if (readRecord.isPresent()) {
            desiredState = Bla.computeState(readRecord.get());
        }

        Log.d("State", "Current state: " + this.state + ", desired state: " + desiredState);
        if (this.state == null || this.state.getFastingState() != desiredState) {
            JejumState old = this.state == null ? null : this.state.getFastingState();
            this.state = states.get(desiredState);
            this.state.updateElements();

            this.state.fromState(old);
        }
    }

    private void update() {
        Log.v("Update", "Update triggered");
        updateState();

        if (! readRecord.isPresent()) {
            return;
        }

        final int windowLength = java.lang.Math.toIntExact(readRecord.get().getWindowLength());
        int secondsUntilTarget = (int) Bla.secondsUntilTarget(readRecord.get().getTarget());
        int progress = windowLength - secondsUntilTarget;
        progressBar.setProgress(progress);

        String from = Bla.formatDayTime(readRecord.get().getStart());
        startDate.setText(from);

        String targetStr = Bla.formatDayTime(readRecord.get().getTarget());
        targetDate.setText(targetStr);

        countdown.setText(Bla.getCountdown(readRecord.get()));
    }

    private void cancelTimer() {
        Log.i("Timer", "Timer has been canceled");
        if (null != this.timer && ! this.timer.isFinished) {
            this.timer.cancel();
            this.timer = null;
        }
    }

    private void scheduleTimer() {
        Log.i("Timer", "Timer has been scheduled");
        if (!readRecord.isPresent()) {
            return;
        }

        cancelTimer();

        long secondsUntilTarget = Bla.secondsUntilTarget(readRecord.get().getTarget());

        final int windowLength = java.lang.Math.toIntExact(readRecord.get().getWindowLength());

        //ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, windowLength);
        progressBar.setMax(windowLength);
        //animation.setDuration(100); // in milliseconds
        //animation.setInterpolator(new DecelerateInterpolator());
        //animation.start();

        // Perform steps + 1 to nicely finish in the ui
        this.timer = new JejumCountdownTimer(1000 * (secondsUntilTarget + 1), 5000) {
            public void onTick(long millisUntilFinished) {
                update();
            }

            @Override
            public void onFinish() {
                Log.i("Timer", "Timer finished");
                isFinished = true;
                evaluateState();
            }
        };

        this.timer.start();
    }

    private class FastingState implements AppState {
        @Override
        public JejumState getFastingState() {
            return JejumState.FASTING;
        }

        @Override
        public void fromState(JejumState state) {
            if (null == state) {

            } else if (JejumState.RESTING == state) {

            }
        }

        @Override
        public final DialogInterface.OnClickListener getDialogButtonListener() {
            return new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int choice) {
                    switch (choice) {
                        case DialogInterface.BUTTON_POSITIVE:
                            dbHelper.deleteCurrent();
                            fetchLatestEntry();
                            update();
                            cancelScheduledNotifications();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                        case DialogInterface.BUTTON_NEUTRAL:
                            Bla.reschedule(readRecord.get());
                            dbHelper.updateRecord(readRecord.get());
                            fetchLatestEntry();
                            update();
                            break;
                    }
                }
            };
        }

        @Override
        public View.OnClickListener getButtonListener() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                    LayoutInflater inflater = getActivity().getLayoutInflater();

                    // Inflate and set the layout for the dialog
                    // Pass null as the parent view because its going in the dialog layout
                    View v = inflater.inflate(R.layout.editdialog, null);
                    builder.setView(v);

                    builder.setMessage("Interrupt fasting?")
                            .setNeutralButton("Reset starttime", getDialogButtonListener())
                            .setPositiveButton("Yes", getDialogButtonListener())
                            .setNegativeButton("No", getDialogButtonListener());

                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
            };
        }

        public String toString() {
            return new Date().toString() + " " + getFastingState();
        }

        @Override
        public void updateElements() {
            action.setText("Interrupt");
            action.setOnClickListener(getButtonListener());
        }
    }

    private final TimePickerDialog.OnTimeSetListener ll = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int i, int i1) {
            System.out.println("yeah");
        }
    };

    private class RestingState implements AppState {
        private Integer customFastingTime;

        private void setCustomFastingTime(int i) {
            this.customFastingTime = Integer.valueOf(i);
        }

        @Override
        public JejumState getFastingState() {
            return JejumState.RESTING;
        }

        @Override
        public void fromState(JejumState state) {
            if (null == state) {

            } else if (JejumState.FASTING == state) {
                /*if (readRecord.isPresent()) {
                    Record r = readRecord.get();
                    r.setActual(r.getTarget());
                    dbHelper.updateRecord(r);
                    fetchLatestEntry();
                }*/
            }
        }

        @Override
        public final DialogInterface.OnClickListener getDialogButtonListener() {
            return new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int choice) {
                    switch (choice) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Record r = newRecord();
                            dbHelper.insertRecord(r);
                            fetchLatestEntry();
                            update();
                            scheduleNotifications(readRecord.get().getTarget());
                            scheduleTimer();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            new MyDialog(getContext(), ll, 0, 0, true).show();
                            break;
                    }
                }
            };
        }

        @Override
        public View.OnClickListener getButtonListener() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View v = inflater.inflate(R.layout.dialog_start_fasting, null);

                    final NumberPicker hours = v.findViewById(R.id.dialog_start_fasting_hours);
                    hours.setMaxValue(24);
                    hours.setMinValue(4);
                    hours.setValue(Integer.parseInt(preferences.getString(getString(R.string.prefs_fasting_duration), "4")));

                    hours.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                        @Override
                        public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                            setCustomFastingTime(numberPicker.getValue());
                        }
                    });

                    Switch s = v.findViewById(R.id.switch1);
                    s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b) {
                                hours.setVisibility(View.VISIBLE);
                            } else {
                                hours.setVisibility(View.GONE);
                            }
                        }
                    });

                    builder.setView(v);
                    builder.setMessage("Start fasting?")
                            .setPositiveButton("Yes", getDialogButtonListener())
                            .setNegativeButton("No", getDialogButtonListener());

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            };
        }

        public String toString() {
            return new Date().toString() + " " + getFastingState();
        }

        @Override
        public void updateElements() {
            action.setText("Start fasting");
            action.setOnClickListener(getButtonListener());
            progressBar.setProgress(0);
        }

        private Record newRecord() {
            if (null != customFastingTime) {
                Record r = new Record(customFastingTime.intValue() * 60 * 60, Bla.generateStartDate());
                // We need to reset it each time
                customFastingTime = null;
                return r;
            }

            int duration = Integer.valueOf(preferences.getString(getResources().getString(R.string.prefs_fasting_duration),""));
            return new Record(duration * 60, Bla.generateStartDate());
        }
    }



    public class DateTimePickerDialog {
        private ManuallyPickedTargetTime manuallyPickedTarget = new ManuallyPickedTargetTime();
        private DialogInterface.OnClickListener listener;
        public DateTimePickerDialog(DialogInterface.OnClickListener listener) {
            this.listener = listener;
        }

        public ManuallyPickedTargetTime getManuallyPickedTarget() {
            return manuallyPickedTarget;
        }

        public AlertDialog.Builder getBuilder() {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

            LayoutInflater inflater = FastingFragment.this.getLayoutInflater();
            View v = inflater.inflate(R.layout.dialog_define_target, null);


            final AllowedDays possibleDates = Bla.getDaysInRange(readRecord.get().getStart(), readRecord.get().getWindowLength());

            final NumberPicker numberPicker = v.findViewById(R.id.spinner1);
            numberPicker.setMinValue(0);
            numberPicker.setMaxValue(possibleDates.size() - 1);
            numberPicker.setValue(numberPicker.getMaxValue());
            numberPicker.setDisplayedValues(possibleDates.keySet().toArray(new String[possibleDates.size()]));
            numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                    manuallyPickedTarget.day = possibleDates.get(numberPicker.getDisplayedValues()[numberPicker.getValue()]);
                }
            });

            final TimePicker picker = v.findViewById(R.id.dialog_define_target_timepicker);
            picker.setHour(readRecord.get().getTarget().getHour());
            picker.setMinute(readRecord.get().getTarget().getMinute());
            picker.setIs24HourView(true);

            picker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                @Override
                public void onTimeChanged(TimePicker timePicker, int hour, int minute) {
                    manuallyPickedTarget.hour = hour;
                    manuallyPickedTarget.minute = minute;
                }
            });

            Switch s = v.findViewById(R.id.dialog_define_target_switch);
            s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        numberPicker.setVisibility(View.VISIBLE);
                        picker.setVisibility(View.VISIBLE);
                        manuallyPickedTarget.day = possibleDates.get(numberPicker.getDisplayedValues()[numberPicker.getValue()]);
                        manuallyPickedTarget.hour = picker.getHour();
                        manuallyPickedTarget.minute = picker.getMinute();
                        manuallyPickedTarget.useManuallyPickedTarget = true;

                    } else {
                        numberPicker.setVisibility(View.GONE);
                        picker.setVisibility(View.GONE);
                        manuallyPickedTarget.useManuallyPickedTarget = true;
                    }
                }
            });

            builder.setView(v);

            builder.setMessage("Override date?")
                    .setPositiveButton("OK", this.listener);

            return builder;
        }
    }
}
